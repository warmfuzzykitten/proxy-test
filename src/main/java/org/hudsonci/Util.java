/*
 * Copyright (c) 2013 Oracle Corporation.
 * 
 * This software is made available under the MIT License.
 * See the LICENSE file for details.
 */

package org.hudsonci;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Bob Foster
 */
public class Util {

  /**
   * EMPTY String[]. Should be only one.
   */
  public static final String[] EMPTY = new String[0];
  
  /** URL buffer size for copy. We usually copy large files. */
  private static final int URL_BUF_SIZE = 1 << 20; // megabyte at a time; copy costs 3M total
  // TODO Do timing tests to see if smaller buffer would suffice

  /** File buffer size for copy. We usually copy small files. */
  private static final int FILE_BUF_SIZE = 4096;

  /**
   * Return the ith column of a two-dimensional array.
   * @param array
   * @param i column to return
   * @return one-dimensional column array
   */
  public static String[] column(String[][] array, int i) {
    // Assumes the array is rectangular.
    String[] column = new String[array.length];
    int n = 0;
    for (String[] s : array) {
      column[n++] = s[i];
    }
    return column;
  }
  
  public static String[] slice(String[] array, int beg) {
    if (beg < 0)
      throw new IllegalArgumentException(String.format("Begin %d < 0", beg));
    int n = array.length - beg;
    if (n < 0)
      throw new IllegalArgumentException(String.format("Begin %d > array length %d", beg, array.length));
    if (n == 0) return EMPTY;
    String[] slice = new String[n];
    System.arraycopy(array, beg, slice, 0, n);
    return slice;
  }
  
  public static String toString(String[] array) {
    StringBuilder sb = new StringBuilder();
    sb.append("[");
    for (int i = 0; i < array.length; i++) {
      String s = array[i];
      if (i > 0)
        sb.append(", ");
      sb.append('"');
      sb.append(s);
      sb.append('"');
    }
    sb.append("]");
    return sb.toString();
  }
  
  public static void print(String ... args) {
    int i = 0;
    for (String s : args) {
      if (i++ > 0)
        System.out.print(" ");
      System.out.print(s);
    }
    System.out.println();
  }
  
  public static void printf(String format, String... args) {
    print(String.format(format, args));
  }
  
  public static String[] catFileNames(String[] strs, File file) {
    ArrayList<String> list = new ArrayList<String>();
    for (String s : strs)
      list.add(s);
    File[] files = file.listFiles();
    for (File f : files)
      list.add(f.getName());
    return list.toArray(new String[list.size()]);
  }
  
  public static int cmp(String[] a, String[] b) {
    int n = Math.min(a.length, b.length);
    for (int i = 0; i < n; i++) {
      int cmp = a[i].compareTo(b[i]);
      if (cmp != 0)
        return cmp;
    }
    return a.length < b.length ? -1 : (a.length > b.length ? 1 : 0);
  }

  public static int cmp(int[] a1, int[] a2) {
    int len1 = a1.length;
    int len2 = a2.length;
    int min = Math.min(len1, len2);
    for (int i = 0; i < min; i++) {
      if (a1[i] != a2[i])
        return a1[i] < a2[i] ? -1 : 1;
    }
    return len1 < len2 ? -1 : (len1 > len2 ? 1 : 0);
  }
  
  public static void arrayToFile(String[] array, File to) throws IOException {
    PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(to)));
    try {
      for (String s : array) {
        writer.println(s);
      }
    } finally {
      writer.close();
    }
  }
  
  /**
   * Extract version part of tag
   * @param tag e.g., "abc-plugin-0.5-h-1-SNAPSHOT" => "0.5-h-1-SNAPSHOT"
   * @return version
   */
  public static String extractVersion(String tag) {
    Matcher match = Pattern.compile("[0-9]").matcher(tag);
    if (match.find())
      return tag.substring(match.start());
    return null;
  }
  
  /**
   * Get name part of url.
   * @param url string url
   * @return name
   * @throws IOException 
   */
  public static String getName(String url) throws IOException {
    return getName(new URL(url));
  }
  
  /**
   * Get name part of URL.
   * @param url URL
   * @return name
   */
  public static String getName(URL url) {
    String path = url.getPath();
    return new File(path).getName();
  }
  
  /**
   * Copy from URL to file.
   * 
   * @param fromUrl source URL
   * @param toFile destination file or folder
   * @return number of bytes copied
   * @throws IOException 
   */
  public static int copyUrl(String fromUrl, File toFile) throws IOException {
    URL url = new URL(fromUrl);
    if (toFile.isDirectory()) {
      String path = url.getPath();
      toFile = new File(toFile, new File(path).getName());
    }
    InputStream cstream = url.openStream();
    return copy(cstream, toFile, URL_BUF_SIZE);
  }
  
  /**
   * Copy file.
   * 
   * @param from path of file to copy
   * @param to path to copy to; may be a directory
   * @return number of bytes copied
   * @throws IOException 
   */
  public static int copy(File from, File to) throws IOException {
    if (to.isDirectory())
      to = new File(to, from.getName());
    InputStream instream = new FileInputStream(from);
    return copy(instream, to, FILE_BUF_SIZE);
  }
  
  /**
   * Copy InputStream to file.
   * @param from source
   * @param to destination file; may not be a directory
   * @param bufSize buffer size for copy
   * @return number of bytes copied
   * @throws IOException 
   */
  public static int copy(InputStream from, File to, int bufSize) throws IOException {
    // This is dumb code - must be a better way
    byte[] buf = new byte[bufSize];
    BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(to),
                                                        bufSize);
    BufferedInputStream in = new BufferedInputStream(from, bufSize);
    int total = 0;
    try {
      int read = 0;
      while ((read = in.read(buf)) >= 0) {
        out.write(buf, 0, read);
        total += read;
      }
    } finally {
      out.close();
      in.close();
    }
    return total;
  }
  
  public static String pathToFileUrl(File file) throws IOException {
    return pathToFileUrl(file.getCanonicalPath());
  }
  
  public static String pathToFileUrl(String absPath) throws IOException {
    URL url = new URL("file", "", absPath);
    String surl = url.toExternalForm();
    return surl;
  }
}
