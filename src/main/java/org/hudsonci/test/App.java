package org.hudsonci.test;

import java.io.File;
import java.io.IOException;
import org.hudsonci.Util;

/**
 * Simple proxy test.
 */
public class App 
{
  public static final String HUDSON3_UC_URL = "http://hudson-ci.org/update-center3/update-center.json";
  public static final String JENKINS_UC_URL = "http://updates.jenkins-ci.org/update-center.json";
  
  public static void main( String[] args )
  {
    try {
      Util.copyUrl(HUDSON3_UC_URL, new File("hudson-updates"));
    } catch (IOException ex) {
      ex.printStackTrace();
      System.exit(1);
    }
    System.out.println("Copied http://hudson-ci.org/update-center3/update-center.json to hudson-updates");
  }
}
